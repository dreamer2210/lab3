#include <iostream>
#include <fstream>
#include "book.h"
#include "list.h"

#define fileBook "books.txt"
#define fileCons "cons.txt"

using namespace std;

void showMenuTask1();
void showMenuTask2();

bool fillArray(string fileInput);

void addBook();
book findBook(const book& obj);
void clear();
void info();

bool fillq(string fileInput);
void add();
void del();
void info2();

book example(100, 25, "��������", "�����");
list<book> books;
queue<cons> q;

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "ru");

    showMenuTask1();
    showMenuTask2();

    return 0;
}

void showMenuTask1()
{
    book object;

    int action = 1;

    if(!fillArray(fileBook))
        return;

    while(action != 0)
    {
        cout << "������� 1. �������� ��������. (0 ����� �����)" << endl;

        cout << "1. �������� ����� � �������" << endl;
        cout << "2. ������� ����� �� ������� (������������)" << endl;
        cout << "3. ������� ������� ����� �� ������� (��� �����)" << endl;
        cout << "4. ������� ��� ����� �� �������" << endl;
        cout << "5. ��� ���������� � ������ � �������" << endl << endl;

        cin >> action;

        switch (action)
        {
            case 0:
                return;
            case 1:
                addBook();
                break;
            case 2:
                cin >> object;
                cout << findBook(object) << endl;
                break;
            case 3:
                cout << books.get() << endl;
                books.pop();
                break;
            case 4:
                clear();
                break;
            case 5:
                info();
                break;
            default:
                cout << "try again" << endl;
                break;
        }
    }
}

void showMenuTask2()
{
    fillq(fileCons);

    int action = 1;

    while (action != 0)
    {
        cout << "������� 2. �������� ��������. (0 ����� �����)" << endl;

        cout << "1. �������� ����� �� �����" << endl;
        cout << "2. ������� �����" << endl;
        cout << "3. �����" << endl;

        cin >> action;

        switch (action)
        {
        case 0:
            return;
        case 1:
            add();
            break;
        case 2:
            del();
            break;
        case 3:
            info2();
            break;
        default:
            cout << "try again" << endl;
            break;
        }
    }
}

// ������� 1

bool fillArray(string fileInput)
{
    fstream file(fileInput);
    book object;

    string author;
    string name;

    double price;
    size_t pages;

    if(!file.is_open())
        return false;

    while(!file.eof())
    {
        file >> object;

        books.push(object);
    }

    file.close();

    return true;
}

void addBook()
{
    book object;

    cout << "For example:" << endl << endl << example << endl;
    cin >> object;

    books.push(object);
}

book findBook(const book& object)
{
    book temp = books.get();
    book result = temp;

    books.pop();

    if(temp != object)
    {
        result = findBook(object);

        books.push(temp);
    }
    
    return result;
}

void clear()
{
    while(books.getCount() != 0)
        books.pop();
}

void info()
{
    for (int i = books.getCount() - 1; i >= 0; --i)
        cout << books[i] << endl;

    cout << "Count books = " << books.getCount() << endl;
}

// ������� 2

bool fillq(string fileInput) 
{
    fstream file(fileInput);

    size_t count;
    double price;

    if (!file.is_open())
        return false;

    while (!file.eof())
    {
        file >> count >> price;

        pushCons(q, count, price);
    }

    file.close();

    return true;
}

void add()
{
    cons object(0, 0);
    
    cout << "������� ���-��: ";
    cin >> object.count;

    cout << "������� ����: ";
    cin >> object.price;

    q.push(object);
}

void del() 
{
    long int count;
    double price;

    cout << "������� ���-��: ";
    cin >> count;

    cout << "������� ����: ";
    cin >> price;

    popCons(q, count, price);
}

void info2() 
{
    cout << endl;
    cout << "���-�� ������:\t" << q.getCountCons() << endl;
    cout << "���-�� ������� ���� ������:\t" << q.getCountAll() << endl;
    cout << "���-�� ������� ������� ������ � �������:\t" << q.getCountCur() << endl;

    cout << "���� ������ �� ����� � ������� ������:\t" << q.getPriceCur() << endl;
    cout << "����� ����:\t" << q.getPriceAll() << endl;
    cout << "������:\t" << q.getProfit() << endl << endl;
}