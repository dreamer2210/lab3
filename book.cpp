#include "book.h"

ostream& operator << (ostream& stream, const book& obj)
{
    stream << "Author: " << obj.author << std::endl;
    stream << "Name: " << obj.name << std::endl;
    stream << "Price: " << obj.price << std::endl;
    stream << "Pages: " << obj.pages << std::endl;

    return stream;
}

istream& operator >> (istream& stream, book& obj)
{
    stream >> obj.author >> obj.name >> obj.price >> obj.pages;

    return stream;
}
