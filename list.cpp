#include "list.h"

struct cons;

template class Node<book>;

template class list<book>;

template class queue<book>;
template class queue<cons>;

// ����

template<typename TData>
Node<TData>::Node(TData data)
{
    this->data = data;

    this->next = 0;
    this->back = 0;
}

template<typename TData>
Node<TData>::Node()
{
    next = 0;
    back = 0;
}

template<typename TData>
Node<TData>::~Node()
{
    this->next = 0;
    this->back = 0;
}

// ����������� ������ �� �������� ����� (�� ���� �� ����������, �� �������� ��� �����������)

template<typename TData>
list<TData>::list(TData data)
{
    start = new Node<TData>(data);
    current = start;

    count = 1;
}

template<typename TData>
list<TData>::list()
{
    count = 0;
    start = 0;
    current = 0;
}

template<typename TData>
list<TData>::~list()
{
    if(count == 0)
        return;

    while(count != 0)
        pop();
}

template<typename TData>
void list<TData>::push(TData data)
{
    if(!start)
    {
        start = new Node<TData>(data);
        current = start;
    }
    else
    {
        current->next = new Node<TData>(data);
        current->next->back = current;
        current = current->next;
    }

    ++count;
}

template<typename TData>
void list<TData>::pop()
{
    if(count > 1)
    {
        Node<TData>* ptrBack = current->back;
        delete current;
        current = ptrBack;

        ptrBack = 0;
        --count;
    }
    else if(count == 1)
    {
        delete current;
        current = 0;
        start = 0;

        --count;
    }
}

template<typename TData>
const TData& list<TData>::operator [] (int index) const
{
    if(index < 0 || index >= count)
        return current->data;

    Node<TData>* ptr = start;

    for(size_t i = 0; i < index && ptr->next; ++i)
        ptr = ptr->next;
    
    return ptr->data;
}

template<typename TData>
TData& list<TData>::operator [] (int index)
{
    if(index < 0 || index >= count)
        return current->data;

    Node<TData>* ptr = start;

    for(size_t i = 0; i < index && ptr->next; ++i)
        ptr = ptr->next;
    
    return ptr->data;
}

// �������

template<typename TData>
queue<TData>::queue(TData data) 
{
    start = new Node<TData>(data);
    last = start;

    profit = 0;
    priceAll = 0;
    countAll = 0;
    countCons = 0;
    countCur = 0;
}

template<typename TData>
queue<TData>::queue()
{
    start = 0;
    last = 0;
    profit = 0;
    priceAll = 0;
    countAll = 0;
    countCons = 0;
    countCur = 0;
}

template<typename TData>
queue<TData>::~queue()
{
    if (countCons == 0)
        return;

    while (countCons != 0)
        pop();
}

template<typename TData>
void queue<TData>::push(TData data) 
{
    if (!start)
    {
        start = new Node<TData>(data);
        last = start;
    }
    else
    {
        last->next = new Node<TData>(data);
        last->next->back = last;
        last = last->next;
    }

    ++countCons;
}

template<typename TData>
void queue<TData>::pop() 
{
    if (countCons > 1)
    {
        Node<TData>* newStart = start->next;
        delete start;
        start->next = 0;
        start->back = 0;
        start = newStart;
        newStart = 0;

        --countCons;
    }
    else if (countCons == 1)
    {
        delete start;
        start = 0;
        last = 0;

        --countCons;
    }
}

void pushCons(queue<cons>& obj, size_t count, double price)
{
    if (!obj.start)
    {
        obj.start = new Node<cons>;
        obj.start->data.count = count;
        obj.start->data.price = price;
        obj.last = obj.start;

        obj.countCur = count;
        obj.priceCur = price;
    }
    else
    {
        cons object(count, price);

        obj.last->next = new Node<cons>(object);
        obj.last->next->back = obj.last;
        obj.last = obj.last->next;
    }

    obj.priceAll += price * count;
    obj.countAll += count;
    ++obj.countCons;
}

void popCons(queue<cons>& obj, size_t count, double price)
{
    obj.profit += price * count - obj.start->data.price * count;
    obj.priceAll -= obj.start->data.price * count;
    obj.countAll -= count;

    if (obj.start->data.count > count)
    {
        obj.start->data.count -= count;
    }
    else if (obj.start->data.count == count)
    {
        if (!obj.start->next)
        {
            obj.countAll = 0;
            obj.countCons = 0;
            obj.countCur = 0;

            obj.priceAll = 0;
            obj.priceCur = 0;

            delete obj.start;
            return;
        }

        Node<cons>* newStart = obj.start->next;
        delete obj.start;
        obj.start->next = 0;
        obj.start->back = 0;
        obj.start = newStart;
        newStart = 0;
        --obj.countCons;
        obj.priceCur = obj.start->data.price;
    }
    else
    {
        double difCount = count - obj.start->data.count;
        size_t difPrice = price * count - obj.start->data.price * count;

        Node<cons>* newStart = obj.start->next;
        delete obj.start;
        obj.start->next = 0;
        obj.start->back = 0;
        obj.start = newStart;
        newStart = 0;
        --obj.countCons;
        obj.priceCur = obj.start->data.price;
        popCons(obj, difCount, difPrice);
    }

    obj.countCur = obj.start->data.count;
}

template<typename TData>
const TData& queue<TData>::operator [] (int index) const
{
    if (index < 0 || index >= countCons)
        return start->data;

    Node<TData>* ptr = start;

    for (size_t i = 0; i < index && ptr->next; ++i)
        ptr = ptr->next;

    return ptr->data;
}

template<typename TData>
TData& queue<TData>::operator [] (int index)
{
    if (index < 0 || index >= countCons)
        return start->data;

    Node<TData>* ptr = start;

    for (size_t i = 0; i < index && ptr->next; ++i)
        ptr = ptr->next;

    return ptr->data;
}
