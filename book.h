#pragma once

#include <string>
#include <ostream>
#include <istream>

typedef std::string string;
typedef std::ostream ostream;
typedef std::istream istream;

struct book
{
    string name;
    string author;
    double price;
    size_t pages;

    book(double price, size_t pages, string name, string author)
    {
        this->price = price;
        this->pages = pages;
        this->name = name;
        this->author = author;
    }

    book(){};

    book& init(double price, size_t pages, string name, string author)
    {
        this->price = price;
        this->pages = pages;
        this->name = name;
        this->author = author;

        return *this;
    }

    book& operator = (const book& obj)
    {
        this->name = obj.name;
        this->author = obj.author;
        this->price = obj.price;
        this->pages = obj.pages;

        return *this;
    }
    bool operator == (const book& obj)
    {
        if(author == obj.author && name == obj.name)
            return true;
        else
            return false;
    }
    bool operator != (const book& obj)
    {
        return !(*this == obj);
    }

    friend ostream& operator << (ostream& stream, const book& obj);
    friend istream& operator >> (istream& stream, book& obj);
};
