#pragma once

#include "book.h"

struct cons;

template<typename TData>
class Node;

template<typename TData>
class list;

template<typename TData>
class Node
{
public:
    TData data;
    Node<TData>* next;
    // ��� �����������
    Node<TData>* back;

    Node(TData data);
    Node();
    ~Node();
};

template<typename TData>
class list
{
private:
    size_t count;
    Node<TData>* start;
    Node<TData>* current;
public:
    list(TData data);
    list();
    ~list();

    void push(TData value);
    void pop();
    TData& get() 
    { 
        if (current)
            return current->data;
        else
        {
            TData data;
            return data;
        }
    }

    size_t getCount()
    {
        return count;
    }

    const TData& operator [] (int index) const;
    TData& operator [] (int index);
};

template<typename TData>
class queue 
{
private:
    size_t countCons;
    size_t countCur;
    size_t countAll;

    double priceAll;
    double priceCur;
    double profit;

    Node<TData>* start;
    Node<TData>* last;
public:
    queue(TData data);
    queue();
    ~queue();

    void push(TData value);
    void pop();

    friend void pushCons(queue<cons>& object, size_t count, double price);
    friend void popCons(queue<cons>& object, size_t count, double price);

    double getProfit() 
    {
        return profit;
    }
    double getPriceAll()
    {
        return priceAll;
    }
    double getPriceCur() 
    {
        return priceCur;
    }
    size_t getCountCons()
    {
        return countCons;
    }
    size_t getCountCur() 
    {
        return countCur;
    }
    size_t getCountAll() 
    {
        return countAll;
    }

    const TData& operator [] (int index) const;
    TData& operator [] (int index);
};

struct cons
{
public:
    size_t count;
    double price;

    cons(size_t count, double price)
    {
        this->count = count;
        this->price = price;
    }
    cons()
    {
        count = 0;
        price = 0;
    }

    cons& operator = (const cons& obj)
    {
        this->count = obj.count;
        this->price = obj.price;

        return *this;
    }
};

